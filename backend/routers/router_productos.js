const express = require('express');
const router = express.Router();
const controladorProductos = require('../controllers/controller_productos');


//CONFIGURACION DE LA RUTA CON EL METODO <<SUGERIDO>> POR EL PROFESOR
router.get("/listar",controladorProductos);
router.get("/cargar/:id",controladorProductos);
router.post("/agregar",controladorProductos);
router.post("/editar/:id",controladorProductos);
router.delete("/borrar/:id",controladorProductos);

module.exports = router