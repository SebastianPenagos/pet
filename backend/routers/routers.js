const express = require('express');
const router = express.Router();
const controladorProductos = require('./router_productos');
const controladorClientes = require('./router_clientes')

router.use("/productos",controladorProductos);
router.use("/clientes",controladorClientes);


module.exports = router



